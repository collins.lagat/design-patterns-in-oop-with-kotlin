class CaptainLazy private constructor() {
    init {
        println("Captain initialised")
        numberOfInstance++
    }
    companion object {
        @Volatile
        private var instance: CaptainLazy? = null
        private var numberOfInstance = 0

        @Synchronized
        fun getCaptain(): CaptainLazy? {
            val checkInstance = instance
            if (checkInstance == null) {
                instance = CaptainLazy()
            }
            println("Number of instances at this moment=$numberOfInstance")
            return instance
        }
    }
}

class CaptainEager private constructor() {
    init {
        println("Captain initialised")
    }
    companion object {
        private var instance: CaptainEager = CaptainEager()

        fun getCaptain(): CaptainEager {
            return instance
        }

        fun  dummyMethod() {
            println("Dummy Method")
        }
    }
}
class CaptainBillPugh private constructor() {
    init {
        println("Captain initialised")
    }
    companion object {
        fun getCaptain(): CaptainBillPugh {
            return Helper.instance
        }
        fun  dummyMethod() {
            println("Dummy Method")
        }
    }

    private class Helper {
        companion object {
            var instance: CaptainBillPugh = CaptainBillPugh()
        }
    }
}

class CaptainDoubleCheckedLock private constructor(){
    init {
        println("Captain initialised")
        numberOfInstance++
    }
    companion object {
        @Volatile
        private var instance: CaptainDoubleCheckedLock? = null
        private var numberOfInstance = 0

        fun getCaptain(): CaptainDoubleCheckedLock? {
            if (instance == null) {
                synchronized(this) {
                    if (instance == null) {
                        val checkInstance = instance
                        if (checkInstance == null) {
                            instance = CaptainDoubleCheckedLock()
                        }
                    }
                }
            }
            println("Number of instances at this moment=$numberOfInstance")
            return instance
        }
    }
}